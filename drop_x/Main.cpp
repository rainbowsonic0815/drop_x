#include "Main.h"

int WINAPI	WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd) {
	ChangeWindowMode(TRUE), DxLib_Init(), SetDrawScreen(DX_SCREEN_BACK);

	while (ScreenFlip()==0 && ProcessMessage()==0 && ClearDrawScreen()==0) {
		DrawString(0, 0, "HelloWorld!!", GetColor(255, 255, 255));
	}

	DxLib_End();
	return 0;
}